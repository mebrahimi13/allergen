package mebrahimi13.allergen.gui;

import mebrahimi13.allergen.gui.dfa.DfaPannel;
import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class Main
{

   private JFrame frame;

    public static void main(String args[])
    {
        EventQueue.invokeLater(() -> {
            try
            {
                Main window = new Main();
                window.frame.setSize(1200, 300);
                window.frame.setVisible(true);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        });
    }

   Main()
    {
       frame = new JFrame();
       frame.setMinimumSize(new Dimension(1000, 570));
       frame.setBounds(100, 100, 450, 300);
       frame.setDefaultCloseOperation(3);
       DfaPannel dfaPannel = new DfaPannel(frame);
       JTabbedPane tabbedPane_2 = new JTabbedPane(1);
       tabbedPane_2.addTab("Allergen Discriminant Function Analysis", dfaPannel.makeDfaPanel());
       frame.getContentPane().add(tabbedPane_2, "North");
    }

}
