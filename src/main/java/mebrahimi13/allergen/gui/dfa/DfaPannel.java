package mebrahimi13.allergen.gui.dfa;

import mebrahimi13.allergen.dfa.Property;
import mebrahimi13.allergen.gui.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

public class DfaPannel
{
   private Frame frame;
   private JTable table;
   Property properties[];
   DefaultTableModel tableModel;

    public DfaPannel(Frame frame)
    {
       properties = Property.values();
       Object[] columnNames = new Object[properties.length];
       for(int i = 0 ; i < properties.length; i++) columnNames[i] = properties[i].name;
        tableModel = new DefaultTableModel(columnNames, 10);
        this.frame = frame;
        table = getTable();
    }

    public JComponent makeDfaPanel()
    {
        JFileChooser fc = new JFileChooser();
        JPanel panel = new JPanel(false);
        panel.setLayout(new GridBagLayout());
        JScrollPane scrollPane = new JScrollPane(table);
        panel.add(scrollPane, getTableConstraints());
        JLabel infoMessageBar = new JLabel("");
        JButton clearTable = new JButton("Clear Table");
        JButton loadTable = new JButton("Load From File");
        JButton saveTable = new JButton("Write To File");
        JButton addNewRow = new JButton("Add New Row");
        JButton doDfaAnalysis = new JButton("Predict");
        clearTable.addActionListener(new ClearTableActionListener(table, infoMessageBar));
        addNewRow.addActionListener(new AddTableRowActionListener(table));
        loadTable.addActionListener(new LoadTableDataActionListener(frame, table, infoMessageBar, fc));
        saveTable.addActionListener(new WriteTableDataAction(frame, table, infoMessageBar, fc));
        doDfaAnalysis.addActionListener(new RunPredictionActionListener(table, properties));
        panel.add(infoMessageBar, getDfaInfoConstraints());
        panel.add(clearTable, getNormalConstrains(0, 2));
        panel.add(addNewRow, getNormalConstrains(1, 2));
        panel.add(loadTable, getNormalConstrains(2, 2));
        panel.add(saveTable, getNormalConstrains(3, 2));
        panel.add(doDfaAnalysis, getNormalConstrains(5, 2));
        return panel;
    }

    private JTable getTable()
    {
        tableModel.setRowCount(1);
        table = new JTable(tableModel);
        table.setShowGrid(true);
        table.setGridColor(Color.BLACK);
        table.setBorder(new EtchedBorder(0));
        table.setFillsViewportHeight(true);
        TableColumnAdjuster tca = new TableColumnAdjuster(table);
        tca.adjustColumns();
        return table;
    }

    private GridBagConstraints getTableConstraints()
    {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = 2;
        c.weightx = 1.0D;
        c.weighty = 1.0D;
        c.gridwidth = 6;
        c.gridx = 0;
        c.gridy = 1;
        return c;
    }

    private GridBagConstraints getDfaInfoConstraints()
    {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = 2;
        c.gridx = 1;
        c.gridy = 0;
        c.ipady = 40;
        return c;
    }

    private GridBagConstraints getNormalConstrains(int gridx, int gridy)
    {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = gridx;
        c.gridy = gridy;
        c.anchor = 17;
        return c;
    }
}
