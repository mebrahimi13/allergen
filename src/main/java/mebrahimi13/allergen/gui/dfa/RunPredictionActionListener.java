package mebrahimi13.allergen.gui.dfa;

import mebrahimi13.allergen.dfa.Dfa;
import mebrahimi13.allergen.dfa.Property;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;

public class RunPredictionActionListener
    implements ActionListener
{

    public RunPredictionActionListener(JTable table, Property columnNames[])
    {
        this.table = table;
        this.columnNames = columnNames;
    }

    public void actionPerformed(ActionEvent arg0)
    {
        StringBuilder report = new StringBuilder();
        for(int i = 0; i < table.getModel().getRowCount(); i++)
        {
            Map<Property, Double> input = new HashMap<>();
            for(int j = 1; j < table.getModel().getColumnCount() - 1; j++)
            {
               Property key = columnNames[j];
               Object entry = table.getModel().getValueAt(i, j);
              if(entry != null && !entry.toString().equals(""))
              {
                  try
                  {
                      input.put(key, Double.valueOf(entry.toString().trim()));
                  }
                  catch(NumberFormatException e)
                  {
                      report.append((new StringBuilder("Invalid value is specified for ")).append(key).append(" at row ").append(i + 1).toString());
                      report.append("\n");
                  }
              } else
              {
                  report.append((new StringBuilder("Missing value for ")).append(key).append(" at row ").append(i + 1).toString());
                  report.append("\n");
              }
            }

            if(input.size() == columnNames.length - 2)
            {
               Dfa dfa = new Dfa(input);
                report.append((new StringBuilder("row ")).append(i + 1).append(" achieved allergen index ").append(dfa.allergenIndex()).append(" and non-allergen index ").append(dfa.nonAllergenIndex()).toString());
                report.append("\n");
                report.append((new StringBuilder("\t==> row ")).append(i + 1).append(" is ").append(dfa.typeDescriptor()).toString());
                report.append("\n");
                table.getModel().setValueAt(dfa.typeDescriptor(), i, columnNames.length - 1);
            } else
            {
                report.append((new StringBuilder("Missing properties at row ")).append(i + 1).append(". The row was ignored.").toString());
                report.append("\n");
                table.getModel().setValueAt("", i, columnNames.length - 1);
            }
        }

        JTextArea textArea = new JTextArea(report.toString());
        textArea.setColumns(80);
        textArea.setRows(20);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        JOptionPane.showMessageDialog(null, new JScrollPane(textArea), "Report", 1);
    }

    private JTable table;
    private Property columnNames[];
}
