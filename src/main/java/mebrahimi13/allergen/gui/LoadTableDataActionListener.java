package mebrahimi13.allergen.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class LoadTableDataActionListener
    implements ActionListener
{

    public LoadTableDataActionListener(Frame frame, JTable table, JLabel dfaInfo, JFileChooser fc)
    {
        this.frame = frame;
        this.fc = fc;
        tableModel = (DefaultTableModel)table.getModel();
        this.dfaInfo = dfaInfo;
    }

    public void actionPerformed(ActionEvent arg0)
    {
        dfaInfo.setText("");
        int returnVal = fc.showOpenDialog(frame);
        if(returnVal == 0)
        {
            File file = fc.getSelectedFile();
            tableModel.setRowCount(0);
            try
            {
                BufferedReader in = new BufferedReader(new FileReader(file));
                for(String line = in.readLine(); line != null; line = in.readLine())
                {
                    ArrayList row = new ArrayList();
                    String as[];
                    int j = (as = line.split(",")).length;
                    for(int i = 0; i < j; i++)
                    {
                        String entry = as[i];
                        row.add(entry.trim());
                    }

                    tableModel.addRow(row.toArray());
                }

                in.close();
            }
            catch(FileNotFoundException e)
            {
                dfaInfo.setText("File not found.");
            }
            catch(IOException e)
            {
                dfaInfo.setText((new StringBuilder("Problem encountered while loading data from file: ")).append(e.getMessage()).toString());
            }
        }
    }

    private JFileChooser fc;
    private Frame frame;
    private DefaultTableModel tableModel;
    private JLabel dfaInfo;
}
