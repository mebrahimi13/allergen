package mebrahimi13.allergen.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ClearTableActionListener
    implements ActionListener
{

    public ClearTableActionListener(JTable table, JLabel dfaInfo)
    {
        tableModel = (DefaultTableModel)table.getModel();
        this.dfaInfo = dfaInfo;
    }

    public void actionPerformed(ActionEvent arg0)
    {
        dfaInfo.setText("");
        for(int i = 0; i < tableModel.getRowCount(); i++)
        {
            for(int j = 0; j < tableModel.getColumnCount(); j++)
                tableModel.setValueAt("", i, j);

        }

    }

    private JLabel dfaInfo;
    private DefaultTableModel tableModel;
}
