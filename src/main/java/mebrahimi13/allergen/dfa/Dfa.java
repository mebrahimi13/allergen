package mebrahimi13.allergen.dfa;


import java.util.Map;

public class Dfa
{
   double allergen = 0D;
   double nonAllergen = 0D;

   public Dfa(Map<Property, Double> input) {
      for(Map.Entry<Property, Double> p : input.entrySet()) {
         allergen    += p.getKey().allergen    * p.getValue();
         nonAllergen += p.getKey().nonAllergen * p.getValue();
      }
   }

   public boolean isAllergen() {
      return allergen > nonAllergen;
   }

   public String typeDescriptor() {
      return isAllergen() ? "allergen" : "non-allergen";
   }

   public double allergenIndex() {
      return allergen;
   }

   public double nonAllergenIndex() {
      return nonAllergen;
   }
}
