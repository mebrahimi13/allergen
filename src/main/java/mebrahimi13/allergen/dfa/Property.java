package mebrahimi13.allergen.dfa;


public enum Property
{
	
	id      ("Protein ID", -64.54,  -63.00),
   gln_gln ("% Gln-Gln",  -1.655,  -2.426),
   leu     ("% Leu",      -0.199,   0.092),
   gln     ("% Gln",       1.154,   1.281),
   arg     ("% Arg",       1.216,   1.399),
   pro_gly ("% Pro_Gly",   0.376,  -0.323),
   pro_gln ("% Pro-Gln",  -0.622,  -1.421),
   gln_gly ("% Gln-Gly",  -0.807,  -1.571),
   gln_pro ("% Gln-Pro",  -2.315,  -1.582),
   gln_cys ("% Gln-Cys",   2.001,   -0.072),
   glu_pro ("% Glu-Pro",   2.974,    2.061),
   result  ("Result",      1.00D,    1.00D);

   public String name;
   public Double allergen, nonAllergen;

   Property(String name, Double allergen, Double nonAllergen) {
      this.name = name;
      this.allergen = allergen;
      this.nonAllergen = nonAllergen;
   }

}
